# Portal Toolkit plugins
Plugins are a way to add functionality and modify the way the Portal Toolkit application works.
They can be seen as the essence of the application, what gives it life; and in fact,
the core functionality is itself implemented in a plugin!
This allows faster development time as plugins don't necessarily require a restart of the app to load changes.


Standard plugin structure 
-
Plugins follow a strict filesystem structure,
this allows developers to look at other plugins and find a familiar format.

