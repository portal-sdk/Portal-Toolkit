# Portal Toolkit
A modular and pluggable set of tools for the games of the Portal series.


Building
-
To build the application, follow these steps:
```shell
$ git clone https://gitlab.com/portal-sdk/Portal-Toolkit.git # clone repo
$ cd Portal-Toolkit                                          # go into the cloned folder
$ pip install poetry                                         # if you don't already have poetry installed
$ poetry install --no-root --without=docs                    # install only the app dependencies
$ poetry run pyinstaller src/Portal-Toolkit                  # build the application
```
The application executable will now be in `dist/PortalToolkit/`
