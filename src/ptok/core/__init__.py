import srctools.logger

import ptok.window
from ptok.plugin import eventSystem, PluginContainer


def onInitRoot( window: ptok.window.RootWindow ) -> None:
	window.createMenuBarMenu( 'core.menu.file' )
	window.createMenuBarMenu( 'core.menu.settings' )
	window.createMenuBarMenu( 'core.menu.help' )


def onMain( container: PluginContainer ) -> None:
	eventSystem.on( 'ptok.window.root.init', container.manifest.id, onInitRoot )
	srctools.logger.get_logger().info( 'Hello from ptok-core!' )
