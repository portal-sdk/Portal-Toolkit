import re
import typing
from dataclasses import dataclass
from pathlib import Path

import pydantic.typing
from srctools import FileSystem

from . import imporlib


_ID_REGEX: typing.Final[re.Pattern[str]] = re.compile('[a-z0-9_-]{4,}')
_TAG_REGEX: typing.Final[re.Pattern[str]] = re.compile('[a-z0-9_-]{3,}')


class Manifest( pydantic.BaseModel ):
	id: str
	name: str
	description: str
	authors: tuple[ str, ... ]
	tags: tuple[ str, ... ]
	version: str
	entrypoints: dict[ str, str ]
	dependencies: dict[ str, str ]

	class Config:
		allow_mutation = False

	@pydantic.validator( 'id' )
	def _validateId( cls, _id: str ) -> str:
		assert _TAG_REGEX.fullmatch( _id ),  _TAG_REGEX.pattern
		return _id

	@pydantic.validator( 'tags', each_item=True )
	def _validateTags( cls, tag: str ) -> str:
		assert _TAG_REGEX.fullmatch( tag ), ( tag, _TAG_REGEX.pattern )
		return tag


@dataclass( frozen=True )
class PluginContainer:
	path: Path
	contents: FileSystem
	manifest: Manifest
	importer: 'imporlib.Importer'
	inDev: bool
	builtin: bool = False

	def __post_init__( self ) -> None:
		if ( 'builtin' in self.manifest.tags or 'base' in self.manifest.tags ) and not self.builtin:
			raise ValueError( 'External plugins cannot have the "base" or "builtin" tags.' )
