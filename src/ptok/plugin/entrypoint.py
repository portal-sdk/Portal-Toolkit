from dataclasses import dataclass
from typing import Iterable, Self, Iterator

import ptok.plugin.pluginSystem as pSys


@dataclass(frozen=True)
class Entrypoint:
	pluginId: str
	_name: str
	_handlerPath: str

	def __call__( self, *args, **kwargs ) -> None:
		mod = pSys.pluginSystem.getContainer( self.pluginId ).importer.load( pSys.pluginSystem, self._handlerPath )
		getattr( mod, f'on{self._name[0].upper()}{self._name[ 1 :]}' )( *args, **kwargs )


@dataclass( frozen=True )
class EntrypointContainer(Iterable[Entrypoint]):
	_name: str
	_entrypoints: list[ tuple[str, str] ]

	def __iadd__( self, other: tuple[str, str] ) -> Self:
		self._entrypoints.append( other )
		return self

	def __iter__( self ) -> Iterator[Entrypoint]:
		for pluginId, handlerPath in self._entrypoints:
			yield Entrypoint( pluginId, self._name, handlerPath )

	def __call__( self, *args, **kwargs ) -> None:
		for pluginId, handlerPath in self._entrypoints:
			mod = pSys.pluginSystem.getContainer( pluginId ).importer.load( handlerPath )
			getattr( mod, f'on{self._name[0].upper()}{self._name[ 1 :]}' )( *args, **kwargs )
