"""
Module used to listen to and emit events.
events are distinguished by an id/name with is usually in the form of `$plugin.$what[.$name].$event`,
for example: `ptok.window.root.init` could be roughly translated to `event init of the window named root from ptok`.

Event handler _should not_ return anything, as the return value is scrapped.
"""
import typing
from dataclasses import dataclass

import srctools


def emit( _evt: str, /, **kwargs ) -> None:
	""" Emits an event. """
	if _evt in __listeners:
		__logger.debug( f'invoked {_evt} with kwargs={kwargs} on {len( __listeners[_evt ] )} listeners' )
		for listener in __listeners[_evt ]:
			listener( **kwargs )
	else:
		__logger.debug( f'invoked {_evt} with kwargs={kwargs} on 0 listeners' )


def on( evt: str, owner: str, listener: typing.Callable[[...], None] ) -> None:
	"""
	Registers an event listener from plugin $owner for the event $evt.
	When the event is `emit()`ted the $listener callable will be invoked.
	"""
	if evt in __listeners:
		__listeners[evt].append( Listener( owner, listener ) )
	else:
		__listeners[evt] = [ Listener( owner, listener ) ]


@dataclass
class Listener:
	owner: str
	callback: typing.Callable[ [...], None ]

	def __call__( self, **kwargs ) -> None:
		self.callback( **kwargs )


__listeners: dict[ str, list[ Listener ] ] = { }
__logger = srctools.logger.get_logger( 'eventSystem' )
