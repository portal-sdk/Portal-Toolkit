import abc
import logging
import types
from importlib.abc import SourceLoader, InspectLoader, PathEntryFinder, Loader
from importlib.machinery import ModuleSpec
from typing import Mapping, Any, Sequence, TYPE_CHECKING

import srctools.logger
from importlib.abc import TraversableResources, Traversable

import ptok.plugin.pluginSystem as pSys


class Importer( SourceLoader, InspectLoader, TraversableResources, PathEntryFinder, metaclass=abc.ABCMeta ):
	""" "Small" abstract class to overwrite the concept of `import` for plugins """
	_logger: logging.Logger

	def __init__( self ) -> None:
		self._logger = srctools.logger.get_logger( self.__class__.__name__ )

	@abc.abstractmethod
	def load( self, plugSys: 'pSys.PluginSystem', path: str ) -> types.ModuleType:
		""" Loads a module with this importer, taking care of everything """

	def tryFind( self, path: str ) -> types.ModuleType | None:
		try:
			return self.load( pSys.pluginSystem, path )
		except:
			return None

	def find_spec( self, fullname: str, target: types.ModuleType | None = ... ) -> ModuleSpec | None:
		return ModuleSpec(
			name=fullname,
			loader=self,
			origin=self.get_filename( fullname ),
			loader_state=None,
			is_package=self.is_package( fullname )
		)

	def find_loader( self, fullname: str ) -> tuple[ Loader | None, Sequence[ bytes | str ] ]:
		self._logger.warning( f'Someone tried to call Importer::find_loader({repr(fullname)}), this is not supported!' )
		return None, []

	def find_module( self, fullname: str ) -> Loader | None:
		self._logger.warning( f'Someone tried to call Importer::find_module({repr(fullname)}), this is not supported!' )
		return None

	@abc.abstractmethod
	def invalidate_caches( self ) -> None:
		...

	@abc.abstractmethod
	def get_data( self, path: str ) -> bytes:
		"""
		Abstract method which returns the bytes for the specified path.
		:param path: path of the file to read the data from.
		:returns: the bytes read for that file.
		"""

	@abc.abstractmethod
	def set_data( self, path: bytes | str, data: bytes ) -> None:
		...

	@abc.abstractmethod
	def get_filename( self, fullname: str ) -> bytes | str:
		...

	@abc.abstractmethod
	def is_package( self, fullname: str ) -> bool:
		...

	@abc.abstractmethod
	def path_stats( self, path: bytes | str ) -> Mapping[ str, Any ]:
		...

	@abc.abstractmethod
	def files( self ) -> Traversable:
		...

	@abc.abstractmethod
	def has( self, fullname: str ) -> bool:
		...


from .sandboxedImporter import SandboxedImporter
