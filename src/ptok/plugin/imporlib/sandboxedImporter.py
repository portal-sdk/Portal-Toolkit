import importlib.util
import os
import types
from time import time
from typing import Mapping, Any, TYPE_CHECKING

from importlib.abc import Traversable
from srctools import FileSystem

from . import Importer
if TYPE_CHECKING:
	from ..pluginSystem import PluginSystem
from ...util import TODO


class SandboxedImporter( Importer ):
	""" "Small" class to overwrite the concept of `import` for plugins """
	_fs: FileSystem
	_inDev: bool
	_cache: dict[ str, types.ModuleType ]

	def __init__( self, inDev: bool, fs: FileSystem ) -> None:
		super().__init__()
		self._fs = fs
		self._inDev = inDev
		self._cache = { }

	def load( self, plugSys: 'PluginSystem', path: str ) -> types.ModuleType:
		if path in self._cache:
			return self._cache[path]

		base: types.ModuleType | None = None
		if '.' in path:
			base = self.load( plugSys, '.'.join( path.split('.')[:-1] ) )

		spec = self.find_spec( path )
		mod: types.ModuleType | None = self.create_module( spec )
		if mod is None:
			mod = importlib.util.module_from_spec( spec )

		# init dunder values
		mod.__name__ = spec.name
		mod.__file__ = spec.origin
		mod.__cached__ = None  # always None, as packaged plugins are already compiled, and in-dev plugins are expected to change
		if self.is_package( path ):
			mod.__path__ = [ os.path.dirname( self.get_filename( path ) ) ]
			mod.__package__ = mod.__name__
		else:
			mod.__package__ = os.path.dirname( self.get_filename( path ) )
		mod.__loader__ = self
		# overwrite the import mechanism
		mod.__builtins__ = { **__builtins__, '__import__': plugSys._import, '_imp': __import__ }

		self.exec_module( mod )

		self._cache[path] = mod
		if base:
			setattr( base, path.split('.')[-1], mod )

		return mod

	def invalidate_caches( self ) -> None:
		self._cache.clear()

	def get_data( self, path: bytes | str ) -> bytes:
		with self._fs.open_bin( path ) as file:
			return file.read()

	def set_data( self, path: bytes | str, data: bytes ) -> None:
		# we don't support writing to files as of now
		return

	def get_filename( self, fullname: str ) -> bytes | str:
		path = fullname.replace( '.', '/' ) + ( '/__init__.py' if self.is_package( fullname ) else '.py' )

		if path in self._fs:
			return path
		raise ImportError( f'{path} was not found.' )

	def is_package( self, fullname: str ) -> bool:
		path = fullname.replace( '.', '/' )
		if f'{path}/__init__.py' in self._fs or f'{path}/__init__.pyc' in self._fs:
			return True
		if f'{path}.py' in self._fs or f'{path}.pyc' in self._fs:
			return False
		raise ImportError( f'{path} does not exist.' )

	def path_stats( self, path: bytes | str ) -> Mapping[ str, Any ]:
		return { 'mtime': time() }

	def files( self ) -> Traversable:
		TODO()
		# return importlib.resources.abc.Traversable()

	def has( self, fullname: str ) -> bool:
		path = fullname.replace( '.', '/' )
		return f'{path}/__init__.py' in self._fs or f'{path}.py' in self._fs
