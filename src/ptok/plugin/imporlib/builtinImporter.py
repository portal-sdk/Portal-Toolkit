import collections.abc
import importlib
import os
import types
import typing

from importlib.abc import Traversable

from . import Importer

if typing.TYPE_CHECKING:
	from ..pluginSystem import PluginSystem
from ...util import TODO


class BuiltinImporter( Importer ):
	""" Tiny wrapper around the standard import mechanism. """
	_root: str

	def __init__( self, package: str ) -> None:
		super().__init__()
		self._root = package

	def load( self, plugSys: 'PluginSystem', path: str ) -> types.ModuleType:
		self._checkRoot( path )
		# FIXME: This will give an empty module when frozen, find out why
		return importlib.import_module( path )

	def invalidate_caches( self ) -> None:
		TODO()

	def get_data( self, path: bytes | str ) -> bytes:
		TODO()

	def set_data( self, path: bytes | str, data: bytes ) -> None:
		TODO()

	def get_filename( self, fullname: str ) -> bytes | str:
		self._checkRoot( fullname )
		return fullname.replace( '.', '/' ) + '.py'

	def is_package( self, fullname: str ) -> bool:
		self._checkRoot( fullname )
		x = __import__( fullname )
		return os.path.basename( x.__file__ ) == '__init__.py'

	def path_stats( self, path: bytes | str ) -> collections.abc.Mapping[ str, typing.Any ]:
		TODO()

	def files( self ) -> Traversable:
		TODO()

	def has( self, fullname: str ) -> bool:
		try:
			self._checkRoot( fullname )
			__import__( fullname )
			return True
		except ImportError:
			return False

	def _checkRoot( self, path: str ) -> None:
		if not path.startswith( self._root ):
			raise ImportError( f'Path outside of root package: {path}' )
