import abc
import typing
import logging

import srctools.logger
import PySide6.QtGui

import ptok.window
import ptok.plugin.eventSystem


class Plugin( metaclass=abc.ABCMeta ):
	_container: 'ptok.plugin.PluginContainer'
	logger: typing.Final[logging.Logger]

	def __init__( self, container: 'ptok.plugin.PluginContainer' ) -> None:
		self._container = container
		self.logger = srctools.logger.get_logger( self.id )
		ptok.plugin.eventSystem.on( "ptok.window.root.init", self.id, self.onWindowInit )
		ptok.plugin.eventSystem.on( "ptok.window.root.close", self.id, self.onWindowClose )

	@property
	def id( self ) -> str:
		""" The plugin's id, as declared in the manifest. """
		return self._container.manifest.id

	@property
	def name( self ) -> str:
		""" The plugin's name, as declared in the manifest. """
		return self._container.manifest.name

	def onWindowInit( self, window: ptok.window.RootWindow ) -> None:
		"""
		The handler for the event generated by the root window when its being initialized.\n
		You can use this event, for example, to add your components to the window,
		to initialize some other ui window or to do initialization in general.
		\t
		:param window: The root window's instance.
		"""

	def onWindowClose( self, window: ptok.window.RootWindow, evt: PySide6.QtGui.QCloseEvent ) -> None:
		"""
		The handler for the event generated by the root window when its being closed.\n
		You can use this event, for example, to perform the unloading of resources,
		to save some component's state, or to even disallow the closing of the window.
		\t
		:param window: The root window's instance.
		:param evt: The event generated by Qt.
		"""

