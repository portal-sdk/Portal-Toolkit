import logging
import os
import random
import types
import typing
import zipfile
from pathlib import Path

import pydantic
import tomlkit
import srctools.filesys

import ptok.config
from .entrypoint import EntrypointContainer
from .pluginContainer import PluginContainer, Manifest


class PluginSystem:
	_logger: logging.Logger
	_containers: list[ PluginContainer ]
	_entrypoints: dict[ str, EntrypointContainer ]

	def __init__( self ) -> None:
		self._logger = srctools.logger.get_logger( 'PluginSystem' )

	@property
	def pluginFolder( self ) -> Path:
		return Path( './plugins' )

	def getEntrypoints( self, name: str ) -> EntrypointContainer:
		return self._entrypoints[ name ]

	def getContainer( self, pluginId: str ) -> PluginContainer:
		for container in self._containers:
			if container.manifest.id == pluginId:
				return container
		raise KeyError( f'Plugin with id `{pluginId}` was not found!' )

	def pluginTable( self ) -> str:
		tableEntries: list[ tuple[ str, str, str, str ] ] = [ ]  # name, id, version, file
		nameSize: int = 4
		idSize: int = 2
		verSize: int = 7
		pathSize: int = 4

		for plugin in self._containers:
			man = plugin.manifest
			nameSize = max( nameSize, len( man.name ) )
			idSize = max( idSize, len( man.id ) )
			verSize = max( verSize, len( man.version ) )
			pathSize = max( pathSize, len( str( plugin.path ) ) )
			tableEntries.append( ( man.name, man.id, man.version, str( plugin.path ) ) )

		def spaces( text: str, size: int ) -> str:
			return f' {text}{" " * ( size - len( text ) )} '

		table = f'{spaces( "Name", nameSize )}|{spaces( "Id", idSize )}|{spaces( "Version", verSize )}|{spaces( "Path", pathSize )}|\n'
		table += f' {"-" * nameSize} | {"-" * idSize} | {"-" * verSize} | {"-" * pathSize} |'
		for name, id_, ver, path in tableEntries:
			table += f'\n{spaces( name, nameSize )}|{spaces( id_, idSize )}|{spaces( ver, verSize )}|{spaces( path, pathSize )}|'

		return table

	def init( self ) -> None:
		"""
		Initializes the plugin system by executing these steps.
		 - [x] Search for possible plugins in the `./plugins/` folder
		 - [x] Load the manifests
		 - [x] Add builtin plugins
		 - [ ] Check dependencies
		 - [x] Create entrypoint containers
		"""
		candidates = self._search()
		containers = self._load( candidates )
		random.shuffle( containers )
		containers += self._loadBuiltins()
		containers.reverse()  # make sure builtins are the first plugins
		self._containers = containers

		self._checkDependencies( containers )
		self._entrypoints = self._loadEntrypoints( containers )
		self._logger.info( f'Loaded {len( self._containers )} plugins\n{{}}', self.pluginTable() )

	def _search( self ) -> list[Path]:
		self.pluginFolder.mkdir( exist_ok=True )
		candidates = [  ]
		# folders
		for path in self.pluginFolder.glob( '*' ):
			if not ( path.is_dir() and ( path / 'plugin.toml' ).exists() ):
				continue
			candidates.append( path )
			self._logger.debug( f'Found candidate {path.name} ( in-dev plugin, will enable dev mode for it )' )
		# packed
		for path in self.pluginFolder.glob( '*.pt-plugin' ):
			try:
				with zipfile.ZipFile( path ) as zipped:
					zipped.getinfo( 'plugin.toml' )
				candidates.append( path )
				self._logger.debug( f'Found candidate {path.name}' )
			except ( KeyError, zipfile.BadZipfile ):
				self._logger.warning( f'Bad plugin file `{path}`: please remove or fix.' )
		# environment variable ( folder-only )
		for rawPath in os.environ.get( 'PT_PLUGINS', '' ).split(','):
			path = Path( rawPath )
			if not ( path.is_dir() and ( path / 'plugin.toml' ).exists() ):
				self._logger.warning( f'Bad plugin file passed via environment variable `{path}`: please remove or fix.' )
				continue
			candidates.append( path )
			self._logger.debug( f'Found candidate {path.name} ( in-dev plugin, will enable dev mode for it )' )
		return candidates

	def _load( self, candidates: list[Path] ) -> list[PluginContainer]:
		containers = [ ]
		for candidate in candidates:
			fs: srctools.FileSystem
			if candidate.is_dir():
				fs = srctools.filesys.RawFileSystem( candidate )
			else:
				fs = srctools.filesys.ZipFileSystem( candidate, zipfile.ZipFile( candidate ) )

			# noinspection PyBroadException
			try:
				with fs.open_str( 'plugin.toml' ) as file:
					doc = tomlkit.parse( file.read() )

				manifest = Manifest( **doc['plugin'].unwrap() )
				self._logger.debug( f'Created manifest for `{manifest.id}`' )
			except pydantic.ValidationError as e:
				for err in e.errors():  # type: pydantic.error_wrappers.ErrorDict
					if 'tags' in err['loc']:
						tag, regexp = eval( err['msg'] )  # type: str, str
						self._logger.error( f'Plugin located at {candidate.resolve()} has an invalid tag: `{tag}` ( tags must match the regex `{regexp}` )' )
						err['msg'] = f'Invalid tag `{tag}`'
					elif 'id' in err['loc']:
						regexp = err['msg']
						self._logger.error( f'Plugin located at {candidate.resolve()} has an invalid id: `{doc["plugin"]["id"]}` ( IDs must match the regex `{regexp}` )' )
						err['msg'] = f'Invalid id `{doc["plugin"]["id"]}`'
				if ptok.config.get().fastFailDuringLoad:
					raise
				continue
			except Exception:
				self._logger.exception( 'Error during manifest loading:' )
				if ptok.config.get().fastFailDuringLoad:
					raise
				continue

			try:
				importer = ptok.plugin.imporlib.SandboxedImporter( candidate.is_dir(), fs )
				self._logger.debug( f'Created Importer for `{manifest.id}`' )
			except Exception as e:
				self._logger.exception( 'Error during manifest loading:', e )
				if ptok.config.get().fastFailDuringLoad:
					raise
				continue

			self._logger.debug( f'Created container for `{manifest.id}`' )
			containers.append( PluginContainer( candidate, fs, manifest, importer, candidate.is_dir() ) )
		return containers

	def _loadBuiltins( self ) -> list[PluginContainer]:
		from .imporlib.builtinImporter import BuiltinImporter
		import ptok.util
		# TODO: Provide a FileSystem implementation capable of loading from the pyinstaller exec
		fs = srctools.filesys.VirtualFileSystem({ })

		basePath = Path( ptok.__file__ ).parent.parent
		base = PluginContainer(
			path=basePath,
			contents=fs,
			manifest=Manifest(
				id='portal-toolkit',
				name='Portal Toolkit',
				description='A modular and pluggable set of tools for the games of the Portal series.',
				authors=( 'ENDERZOMBI102 <enderzombi102.end@gmail.com>', ),
				tags=( 'builtin', 'base' ),
				version='0.1.0',
				entrypoints={ },
				dependencies={ },
			),
			importer=BuiltinImporter( 'ptok' ),
			inDev=not ptok.util.isFrozen(),
			builtin=True
		)

		doc = tomlkit.parse( ( Path( __file__ ).parent / '../core/plugin.toml' ).resolve().read_text() )
		core = PluginContainer(
			path=basePath,
			contents=fs,
			manifest=Manifest( **doc['plugin'].unwrap() ),
			importer=BuiltinImporter( 'ptok.core' ),
			inDev=not ptok.util.isFrozen(),
			builtin=True
		)

		self._logger.debug( f'Loaded 2 builtin plugins ( base, core )' )
		return [ core, base ]

	def _checkDependencies( self, containers: list[PluginContainer] ) -> None:
		env = { cont.manifest.id: cont.manifest.version for cont in containers }
		errors: list[tuple[str, str, str]] = []  # dependantId, dependencyId, dependencyVer

		for container in containers:
			for depId, depVer in container.manifest.dependencies.items():
				if depId not in env:
					errors.append( ( container.manifest.id, depId, depVer ) )
					continue
				if env[depId]:
					...

	def _loadEntrypoints( self, containers: list[PluginContainer] ) -> dict[str, EntrypointContainer]:
		entrypoints = { }
		for plugin in containers:
			for key, path in plugin.manifest.entrypoints.items():
				if key not in entrypoints:
					entrypoints[ key ] = EntrypointContainer( key, [] )
				entrypoints[key] += ( plugin.manifest.id, path )
		self._logger.info( f'Loaded {len(entrypoints)} entrypoints' )
		return entrypoints

	# noinspection PyShadowingBuiltins
	def _import( self, name: str, globals: typing.Mapping[str, typing.Any] | None = ..., locals: typing.Mapping[str, typing.Any] | None = ..., fromlist: typing.Sequence[str] = ..., level: int = ... ):
		# handle relative imports
		resolved: str
		if level:
			parts: list[str] = globals['__name__'].split('.')
			resolved = '.'.join( parts[ : -level ] )
			if name:
				resolved += f'.{name}'
		else:
			resolved = name

		self._logger.debug( 'Module {} is searching for {}', globals['__name__'], resolved )

		for plugin in self._containers:
			if mod := plugin.importer.tryFind( resolved ):
				# handle `from A.B import c`
				if '.' in resolved and not fromlist:
					mod = plugin.importer.tryFind( resolved.split('.')[0] )
				# handle `from pkg import module`
				if fromlist:
					for item in fromlist:
						if plugin.importer.has( f'{resolved}.{item}' ):
							setattr( mod, item, plugin.importer.load( self, f'{resolved}.{item}' ) )

				self._logger.debug( ' - found in plugin `{}`', plugin.manifest.id )
				return mod

		# use sys.path
		try:
			self._logger.debug( ' - not found in any plugin, using og import' )
			return __import__( name, globals, locals, fromlist, level )
		except ImportError as e:
			frame = e.__traceback__.tb_frame.f_back
			raise e.with_traceback( types.TracebackType( None, frame, frame.f_lasti, frame.f_lineno ) )


pluginSystem = PluginSystem = PluginSystem()
