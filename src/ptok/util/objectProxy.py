import collections.abc
import typing


_KT = typing.TypeVar('_KT')
_VT = typing.TypeVar('_VT')
_KT_co = typing.TypeVar( '_KT_co', covariant=True )
_VT_co = typing.TypeVar( '_VT_co', covariant=True )


class WriteProxiedDict( typing.Generic[_KT, _VT], collections.abc.Mapping[_KT, _VT] ):
	_obj: collections.abc.Mapping[_KT, _VT]
	_proxy: collections.abc.MutableMapping[_KT, _VT]

	def __init__( self, obj: collections.abc.Mapping[ _KT, _VT ], proxy: collections.abc.MutableMapping[ _KT, _VT ] ) -> None:
		self._obj = obj
		self._proxy = proxy
		super().__init__()

	def __setitem__( self, key: _KT, value: _VT ) -> None:
		self._proxy[ key ] = value

	def __getitem__( self, item: _KT ) -> _VT_co:
		if item in self._proxy:
			return self._proxy[ item ]

		return self._obj[ item ]

	def __len__( self ) -> int:
		return len({ *self._proxy.keys(), *self._obj.keys() })

	def __iter__( self ) -> typing.Iterator[ _KT_co ]:
		return iter({ *self._proxy.keys(), *self._obj.keys() })

	def __repr__( self ) -> str:
		return repr({ **self._obj, **self._proxy })

	def __str__( self ) -> str:
		return str({ **self._obj, **self._proxy })


# register how to pprint WriteProxiedDict, done as lambda call, so we don't pollute the module namespace
(
	lambda: (
		pprint := __import__( 'pprint' ),
		pprint.PrettyPrinter._dispatch.__setitem__( WriteProxiedDict.__repr__, pprint.PrettyPrinter._pprint_dict )
	)
)()

