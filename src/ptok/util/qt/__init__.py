""" Package containing various utilities to handle and use Qt6 classes. """
import PySide6.QtWidgets


def modalMessageBox( title: str, content: str ) -> None:
	"""
	A simple utility function to create a text-only modal message box.
	\t
	:param title: The title of the message box.
	:param content: The text the message box will display.
	"""
	box = PySide6.QtWidgets.QMessageBox()
	box.setWindowTitle( title )
	box.setText( content )
	box.exec()
