import sys
import traceback
import types
import typing
from pprint import pprint

from ptok.util.objectProxy import WriteProxiedDict


class Console:
	_locals: dict[ str, typing.Any ]
	_globals: dict[ str, typing.Any ]
	_currentTracebackEntry: types.TracebackType
	_tracebackEntries: list[types.TracebackType]
	_buffer: str
	_cause: Exception
	_userVars: dict[ str, typing.Any ]

	def __init__( self, exc: Exception ) -> None:
		self._cause = exc
		self._buffer = ''
		self._userVars = { }

		self._tracebackEntries = [ exc.__traceback__ ]
		last = exc.__traceback__
		while last.tb_next is not None:
			last = last.tb_next
			self._tracebackEntries.append( last )

		self._currentTracebackIndex = len( self._tracebackEntries ) - 1
		self._currentTracebackEntry = last
		self._locals = last.tb_frame.f_locals
		self._globals = last.tb_frame.f_globals

	# region properties
	@property
	def file( self ) -> str:
		return self._globals['__file__']

	@property
	def module( self ) -> str:
		return self._globals['__name__']

	@property
	def clazz( self ) -> str | None:
		return self._locals['self'].__class__.__name__ if 'self' in self._locals else None

	@property
	def function( self ) -> str:
		return self._currentTracebackEntry.tb_frame.f_code.co_name

	@property
	def line( self ) -> int:
		return self._currentTracebackEntry.tb_lineno

	@property
	def globals( self ) -> typing.Mapping[ str, typing.Any ]:
		if '__builtins__' not in self._globals:
			return self._globals

		return { **self._globals, '__builtins__': '<hidden>' }

	@property
	def locals( self ) -> typing.Mapping[ str, typing.Any ]:
		if '__builtins__' not in self._locals:
			return self._locals

		return { **self._locals, '__builtins__': '<hidden>' }

	@property
	def writableLocals( self ) -> typing.Mapping[ str, typing.Any ]:
		return WriteProxiedDict( { **self.locals, '__builtins__': '<hidden>' }, self._userVars )
	# endregion properties

	def loadFrame( self, index: int ) -> None:
		self._currentTracebackIndex = index
		self._currentTracebackEntry = self._tracebackEntries[ index ]
		self._locals = self._currentTracebackEntry.tb_frame.f_locals
		self._globals = self._currentTracebackEntry.tb_frame.f_globals

	def handleCommand( self ) -> None:
		match self._buffer.split( '\n', maxsplit=1 )[0][ 1 :].split( ' ' ):
			case [ 'help' ]:
				print( 'Crash console help' )
				print( '  .help: Shows this message' )
				print( '  .exit: Exits the console' )
				print( '  .clear: Clears the screen' )
				print( '  .list [globals|locals|user_vars]: Shows the given context variable' )
				print( '  .noop: Does nothing ( NO OPeration )' )
				print( '  .about: Prints information on how to use the console' )
				print( '  .move [up|down|to $frameIndex]: Move to a different stack frame' )
				print( '  .frame: Print information about the current stack frame' )
			case [ 'exit' ]:
				sys.exit( 2 )
			case [ 'clear' ]:
				sys.stdout.write( '\033[2J\033[H' )
			case [ 'list', what ]:
				obj: typing.Mapping[ str, typing.Any ]
				if what == 'locals':
					obj = self.locals
				elif what == 'user_vars':
					obj = self._userVars
				elif what == 'globals':
					obj = self.globals
				else:
					sys.stderr.write( f'list: Unknown context variable: {what}\n' )
					return
				pprint( obj )
			case [ 'noop' ]: ...
			case [ 'about' ]:
				print( 'About the crash debug console\n'
				       '  this environment is used to help understanding the state in which the toolkit was in during a crash,\n'
				       '  here you have various tools at your disposal, mainly, a python executor, which supports-\n'
				       '  blocks, multi-line expressions, function calls, etc. and various console commands,\n'
					   '  of which you can learn more via the .help command.\n'
					   '  > So it is a debugger! ~thedarkbomber' )
			case [ 'move', 'up' ]:
				index: int = self._currentTracebackIndex - 1
				if index == -1:
					sys.stderr.write( 'move: Cannot move up: already at first frame\n' )
					return
				self.loadFrame( index )
			case [ 'move', 'down' ]:
				index: int = self._currentTracebackIndex + 1
				if index == len( self._tracebackEntries ):
					sys.stderr.write( 'move: Cannot move down: already at last frame\n' )
					return
				self.loadFrame( index )
			case [ 'move', 'to', where ] if where.isnumeric():  # type: where: str
				index: int = int( where )
				length: int = len( self._tracebackEntries )
				if not 0 < index < length:
					sys.stderr.write( f'move: Cannot move to specified frame: parameter not in valid range (0-{length})\n' )
					return
				self.loadFrame( index )
			case [ 'move', 'to', _ ]:
				sys.stderr.write( 'move: Cannot move to specified frame: parameter not a number\n' )
			case [ 'move', *args ]:
				sys.stderr.write( f'move: Unknown subcommand: `{" ".join( args )}\n' )
			case [ 'frame' ]:
				frame = self._currentTracebackEntry.tb_frame
				print( 'Current frame information:\n'
					   f'  line: {frame.f_lineno}\n'
					   f'  file: {frame.f_globals["__file__"]}\n'
					   f'  function: {self.module}.{frame.f_code.co_qualname}\n'
					   f'  locals: {self.locals}\n'
					   f'  globals: {self.globals}\n')
				# $file :: ( $class (:: $class)* ) :: ( $func (:: $func)* )
			case cmd:
				sys.stderr.write( f'console: Unknown command: `.{" ".join( cmd )}`\n' )

	def run( self ) -> None:
		print( 'Portal Toolkit - Crash console' )
		print( 'type `.help` for help, type `.exit` to exit' )
		clazz = f'{self.clazz}::' if self.clazz else ''
		print( f'crash originated in {self.module}.{clazz}{self.function} at line {self.line} with exception `{type(self._cause).__name__}: {self._cause}`' )

		block: bool = False
		while True:
			try:
				line = input( '... ' if block else '>>> ' )

				# are we entering a block?
				if line.endswith( ':' ):
					block = True

				# handle multi-line expressions
				if line.endswith( '\\' ):
					while line.endswith( '\\' ):
						self._buffer += line[: -1 ]
						line = input( '... ' ).strip()

				self._buffer += line

				# is the input ended and ready to be executed?
				if ( block and not line ) or ( not block and line ):
					# noinspection PyBroadException
					try:
						# input starting with `.` are console commands
						if self._buffer[0] == '.':
							self.handleCommand()
						else:
							executable: str = self._buffer
							# print back handle single-line expressions and assignments
							if not block and not self._buffer.startswith( 'print(' ):
								printable: str
								# try to detect assignments
								if '=' in self._buffer and not ( '(' in self._buffer and self._buffer.index( '=' ) > self._buffer.index( '(' ) ):
									# an assignment has been run
									printable = self._buffer.split( '=', 1 )[0]
									executable = self._buffer
								else:
									# an expression has been run
									printable = self._buffer
								executable += f'\nprint( *[ repr(item) for item in [{printable}] ], sep=", " )'

							exec( executable, self._globals, self.writableLocals )
					except Exception:
						traceback.print_exc()
					self._buffer = ''
					block = False
			except KeyboardInterrupt:
				self._buffer = ''
				print( '\nKeyboardInterrupt: cleared command buffer' )
				block = False
