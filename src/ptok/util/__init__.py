import enum
import inspect
import sys
from pathlib import Path
from typing import NoReturn

from . import lazy


def TODO( message: str | None = None ) -> NoReturn:
	stackFrame = inspect.stack()[1]
	namespace: str
	if 'self' in stackFrame.frame.f_locals:
		namespace = stackFrame.frame.f_locals["self"].__class__.__name__
	else:
		namespace = stackFrame.filename
	message = ( message or '{ctx} is not yet implemented.' ).format( ctx=f'{namespace}::{stackFrame.function}' )
	raise NotImplementedError( message )


def isFrozen() -> bool:
	"""
	If running in a frozen state, returns True
	:returns: True if it is, otherwise false
	"""
	return hasattr( sys, 'frozen' )


@lazy.Lazy
def resourcesFolder() -> Path:
	return ( ( Path.cwd() if isFrozen() else Path.cwd() / '..' ) / 'resources' ).resolve()


class Platform(enum.Enum):
	""" Represents the platform we're running on """
	Windows = enum.auto()
	Linux = enum.auto()
	OSX = enum.auto()

	@staticmethod
	@lazy.Lazy
	def current() -> 'Platform':
		""" The current OS """
		match sys.platform:
			case 'win32':
				return Platform.Windows
			case 'linux':
				return Platform.Linux
			case 'darwin':
				return Platform.OSX
			case os:
				raise NotImplementedError( f'Invalid OS: {os}' )



