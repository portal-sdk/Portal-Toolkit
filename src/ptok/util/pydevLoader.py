import sys
from pathlib import Path

import ptok.util

pydevDir: Path
if ptok.util.isFrozen():
	pydevDir = Path.cwd() / '..' / '..' / 'run' / 'cache' / 'pydev'
else:
	pydevDir = Path.cwd() / 'cache' / 'pydev'


def init():
	if not pydevDir.exists():
		raise RuntimeError()

	sys.path.append( str( pydevDir ) )

	import pydevd
	pydevd.settrace(
		host='127.0.0.1', port=20309,
		suspend=True,
		stdoutToServer=True, stderrToServer=True,
		patch_multiprocessing=True
	)
