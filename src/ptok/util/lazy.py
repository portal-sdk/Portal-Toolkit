import types
import typing

_T = typing.TypeVar( '_T' )
_C = typing.TypeVar( '_C' )
_FLAG = object()


class Lazy( typing.Generic[ _T ] ):
	_getter: typing.Callable[[], _T] | typing.Callable[[type], _T] | typing.Callable[[object], _T]
	_needSelf: bool
	_value: _T

	def __init__( self, getter: typing.Callable[[], _T] | types.FunctionType ) -> None:
		self._value = _FLAG
		self._getter = getter
		self._needSelf = ( not hasattr( getter, '__self__' ) ) and len( getter.__code__.co_varnames ) != 0 and getter.__code__.co_varnames[0] == 'self'

	def __get__( self, instance: _C, owner: type[_C] | None = None ) -> _T:
		if self._value is _FLAG:
			self._value = self._getter( instance ) if self._needSelf else self._getter( owner )

		return self._value

	def __call__( self ) -> _T:
		if self._value is _FLAG:
			self._value = self._getter()

		return self._value
