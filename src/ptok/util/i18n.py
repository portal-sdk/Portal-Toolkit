import logging
import typing
from enum import Enum, auto

import srctools.filesys
import tomlkit.items

import ptok.config

if typing.TYPE_CHECKING:
	import ptok.plugin


def load( containers: list['ptok.plugin.PluginContainer'] ) -> None:
	lang = ptok.config.get().language
	__logger.info( f'Loading translations for ({lang})' )
	# this is so dumb...
	systems = [
		( srctools.filesys.RawFileSystem( ptok.util.resourcesFolder().parent ), 'base' ),
		*[ ( container.contents, container.manifest.id ) for container in containers ]
	]

	res = { }
	path = f'resources/lang/{lang}.toml'
	for fs, plugin in systems:  # type: srctools.FileSystem, str
		if path not in fs:
			__logger.warning( f'Plugin {plugin} offers no translations for the selected language ({lang})' )
			continue
		with fs.open_str( path ) as file:
			__flatten( '', tomlkit.load( file ), res, plugin )

	global __current
	__current = res


def tr( key: str, **kwargs: typing.Any ) -> str:
	"""
	Used to localize strings, keyword arguments may be passed to dynamically change parts of the final text as `{{}}` substitution is done.
	Objects and dictionaries may get either stringified or accessed, based on the text in the localization string.

	`{}` substitution is also done, but with other localized strings.
	"""
	if __current is None:
		raise RuntimeError( 'Called i18n::tr() before translations were loaded!' )

	if key not in __current:
		if ptok.util.isFrozen():
			raise KeyError( f'Translation key `{key}` is missing!' )
		else:
			return key

	return __current[key].expand( **kwargs )


class Translation:
	"""
	A class to contain parsed translated values.
	This is used to speed up localizing a string, as there is no need to parse every time it is localized.
	"""
	class Type(Enum):
		ARGUMENT = auto()
		TRANSLATION = auto()

	_origin: str
	_parts: list[ str | tuple[Type, str] ]

	def __init__( self, string: str, origin: str ) -> None:
		self._origin = origin
		self._parts = [ ]
		# parse the value
		buffer = ''
		char = 0
		while char < len( string ):
			if string[ char ] == '\\' and string[ char + 1 ] == '{':
				buffer += '{'
				char += 1
			elif string[ char ] == '{':
				# append text if present
				if buffer:
					self._parts.append( buffer )
					buffer = ''

				# handle templates
				if string[ char + 1 ] == '{':
					end = string.index( '}}', char )
					self._parts.append( ( Translation.Type.ARGUMENT, string[ char + 2 : end ] ) )
					char = end + 1
				else:
					end = string.index( '}', char )
					self._parts.append( ( Translation.Type.TRANSLATION, string[ char + 1 : end ] ) )
					char = end
			else:
				buffer += string[ char ]
			char += 1

		if buffer:
			self._parts.append( buffer )

	def expand( self, **kwargs: typing.Any ) -> str:
		"""
		Expands this translation with the given kwargs.
		Here `{}` and `{{}}` substitutions are done with respectively other localized strings or by accessing/str() objects and dictionaries.
		"""

		# small optimization for when there is no expansion needed
		if len( self._parts ) == 1 and self._parts[0][0] != '{':
			return self._parts[0]

		buffer = ''
		for entry in self._parts:
			match entry:
				# just text
				case str( text ):
					buffer += text
				# translation expansion
				case ( Translation.Type.TRANSLATION, code ):
					buffer += tr( code, **kwargs )
				# kwargs expansion
				case ( Translation.Type.ARGUMENT, code ):
					parts: list[str] = code.split( '.' )
					# hard error on this one, most likely forgot to pass
					if parts[0] not in kwargs:
						raise ValueError( f'Invalid key in kwargs expansion ( `{parts[0]}` ), available: {", ".join( kwargs.keys() )}' )

					obj: typing.Any = kwargs[ parts[0] ]
					for part in parts[ 1 :]:
						if isinstance( obj, dict ):
							obj = obj.get( part, f'<<missing dict key: {part}>>' )
						else:
							obj = getattr( obj, part, f'<<missing object field: {part}>>' )
					buffer += str( obj )

		return buffer


def __flatten( currentKey: str, source: tomlkit.TOMLDocument, result: dict[str, Translation ], plugin: str ) -> None:
	for entryKey, value in source.items():  # type: str, str | tomlkit.TOMLDocument
		if isinstance( value, tomlkit.items.Table ):
			__flatten( f'{currentKey}.{entryKey}' if currentKey else entryKey, value, result, plugin )
		else:
			if currentKey:
				key = currentKey
				if entryKey != 'value':
					key = f'{key}.{entryKey}'
			else:
				key = entryKey

			result[key] = Translation( value, plugin )


__current: dict[str, 'Translation' ] | None = None
__logger: logging.Logger = srctools.logger.get_logger( 'I18n' )
