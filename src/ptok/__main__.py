import os
import sys
import datetime
import time
from logging import Logger
from pathlib import Path

import srctools.logger
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QApplication, QStyleFactory

import ptok.config
import ptok.util.i18n
import ptok.util.pydevLoader
from ptok.plugin import pluginSystem
from ptok.window import RootWindow


logger: Logger


def main( argv: list[str] ) -> int:
	envMessage: str
	if not ptok.util.isFrozen():
		# avoid running anywhere but inside `run`
		cwd = Path.cwd()
		shouldExit = False
		if cwd.name != 'run':
			shouldExit = True
		elif 'src' in cwd.parts:
			path = Path( cwd.root )
			for part in cwd.parts[ 1 :]:
				if part == 'src':
					break
				path /= part
			shouldExit = ( path / 'run' ).exists()

		if shouldExit:
			print( '[F] __main__.main(): Started with working directory not set to `$REPO/run`, do not do this! aborting', file=sys.stderr )
			return 1

		# overwrite stdout log level if launched from source
		os.environ[ 'SRCTOOLS_DEBUG' ] = '1'
		envMessage = 'Running in a developer environment'
	else:
		envMessage = 'Running in a packed environment'

	if os.environ.get( 'PT_PYDEV', '' ):
		ptok.util.pydevLoader.init()

	global logger
	try:
		logger = srctools.logger.init_logging( filename='./logs/latest.log', main_logger='PortalTk', error=onUncaughtException )
		logger.info( envMessage )
	except Exception:
		print( f'[I] __main__.main(): {envMessage}' )
		raise

	logger.info( f'Initialized logging at {datetime.datetime.now().strftime("%H:%M:%S")}' )
	QApplication( argv )
	QApplication.setStyle( QStyleFactory.create( 'Fusion' ) )
	QApplication.setWindowIcon( QIcon( 'resources/icon.png' ) )
	QApplication.setApplicationName( 'PortalToolkit' )
	QApplication.setApplicationDisplayName( 'Portal Toolkit' )
	QApplication.setApplicationVersion( '0.1.0' )
	# init plugin system and call main entrypoints
	pluginSystem.init()
	failed: int = 0
	for entrypoint in pluginSystem.getEntrypoints( 'main' ):
		# noinspection PyBroadException
		try:
			entrypoint( pluginSystem.getContainer( entrypoint.pluginId ) )
		except Exception:
			logger.exception( f'Uncaught exception during initialization of plugin `{entrypoint.pluginId}`:' )
			failed += 1
			if ptok.config.get().fastFailDuringLoad:
				raise
	del entrypoint
	# handle plugin loading failures
	if failed:
		logger.error( f'{failed} plugins failed to load, aborting' )
		return 1

	ptok.util.i18n.load( pluginSystem._containers )

	root = RootWindow()
	logger.info( 'Startup completed!' )

	code = QApplication.exec()

	ptok.config.save()
	return code


def onUncaughtException( exc: BaseException ) -> None:
	if isinstance( exc, Exception ) and os.environ.get( 'PT_DEBUG', '0' ) == '1':
		sys.stdout.flush()
		sys.stderr.write( '[E] crash.console(): Portal Toolkit crashed while in packed debug mode, starting debug console\n' )
		time.sleep(0.1)
		import ptok.util.crash

		ptok.util.crash.Console( exc ).run()


if __name__ == '__main__':
	sys.exit( main( sys.argv ) )
