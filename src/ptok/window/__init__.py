import PySide6.QtGui
from PySide6.QtWidgets import *

from ..plugin import eventSystem
from ..util.i18n import tr


class RootWindow( QMainWindow ):
	_menus: dict[ str, QMenu ]

	def __init__( self ) -> None:
		super().__init__( parent=None )
		self._menus = { }
		self.setWindowTitle( 'Portal Toolkit' )
		self.setMinimumSize( 500, 600 )

		eventSystem.emit( 'ptok.window.root.init', window=self )

		self.show()

	def createMenuBarMenu( self, translationKey: str ) -> QMenu:
		menu = self._menus[ translationKey.split('.')[-1] ] = self.menuBar().addMenu( tr( f'{translationKey}.title' ) )
		menu.setStatusTip( tr( f'{translationKey}.status-tip' ) )
		return menu

	@property
	def menus( self ) -> dict[str, QMenu]:
		return self._menus

	def menu( self, name: str ) -> QMenu:
		return self._menus[name]

	def closeEvent( self, evt: PySide6.QtGui.QCloseEvent ) -> None:
		eventSystem.emit( 'ptok.window.root.close', window=self, evt=evt )

	def showEvent( self, evt: PySide6.QtGui.QShowEvent ) -> None:
		eventSystem.emit( 'ptok.window.root.shown', window=self )

	def hideEvent( self, evt: PySide6.QtGui.QHideEvent ) -> None:
		eventSystem.emit( 'ptok.window.root.hidden', window=self )
