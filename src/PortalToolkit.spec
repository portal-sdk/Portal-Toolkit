# -*- mode: python ; coding: utf-8 -*-
from PyInstaller.building.api import PYZ, EXE, COLLECT
from PyInstaller.building.build_main import Analysis

block_cipher = None
data = [
	( 'ptok/core/plugin.toml', 'ptok/core' ),
	( '../LICENSE', '.' )
]
hiddenImports = [
	'queue',
	'xmlrpc.client',
	'xmlrpc.server',
	'http.server',
	'ctypes',
	'code',
]

a = Analysis(
	[ 'ptok/__main__.py' ],
	pathex=[ ],
	binaries=[ ],
	datas=data,
	hiddenimports=hiddenImports,
	hookspath=[ ],
	hooksconfig={ },
	runtime_hooks=[ ],
	excludes=[
		'_tkinter',
		'tkinter'
	],
	win_no_prefer_redirects=False,
	win_private_assemblies=True,
	cipher=block_cipher,
	noarchive=False,
)
pyz = PYZ( a.pure, a.zipped_data, cipher=block_cipher )

exe = EXE(
	pyz,
	a.scripts,
	[ ],
	exclude_binaries=True,
	name='PortalToolkit',
	debug=True,
	bootloader_ignore_signals=False,
	strip=False,
	upx=True,
	console=True,
	icon=None,
	disable_windowed_traceback=False,
	argv_emulation=True,
	target_arch=None,
	codesign_identity=None,
	entitlements_file=None,
)
coll = COLLECT(
	exe,
	a.binaries,
	a.zipfiles,
	a.datas,
	strip=False,
	upx=True,
	upx_exclude=[ ],
	name='PortalToolkit',
)
