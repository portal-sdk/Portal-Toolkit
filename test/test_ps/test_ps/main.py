import srctools.logger

from ptok.plugin import PluginContainer


logger = srctools.logger.get_logger( 'test_ps' )


# test entrypoint system
def onMain( container: PluginContainer ) -> None:
	logger.info( f'Plugin `{container.manifest.id}` says hello world! file loc: {__file__}' )


# test absolute imports
import test_ps.pkg.pak
assert test_ps
assert test_ps.__file__ == 'test_ps/__init__.py'
assert test_ps.pkg
assert test_ps.pkg.__file__ == 'test_ps/pkg/__init__.py'
assert test_ps.pkg.pak
assert test_ps.pkg.pak.__file__ == 'test_ps/pkg/pak.py'

# test relative import
from . import hilo
assert hilo
assert hilo.__file__ == 'test_ps/hilo.py'


logger.info( 'PlugSys test completed!' )
