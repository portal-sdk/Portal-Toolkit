from PySide6.QtGui import QAction
from PySide6.QtWidgets import *

import ptok.plugin.eventSystem
from ptok.plugin import PluginContainer
import ptok.window
from ptok.util.i18n import tr


class ExampleMenu:
	_box: QMessageBox | None = None
	_action: QAction

	def onInitRoot( self, window: ptok.window.RootWindow ) -> None:
		self._action = QAction( text=tr( 'test_ui.menu.file.pressed' ) )
		self._action.triggered.connect( self.pressed )
		window.menus['file'].addAction( self._action )

	def pressed( self, something: bool ) -> None:
		if not self._box:
			self._box = QMessageBox(
				QMessageBox.Icon.Information,
				tr('test_ui.mbox.pressed.title'),
				tr('test_ui.mbox.pressed.text')
			)
			self._box.setModal(False)
			self._box.destroyed.connect( lambda s=None: setattr( self, 'box', None ) )
			self._box.show()


__exampleMenu: ExampleMenu = ExampleMenu()


def onMain( container: PluginContainer ) -> None:
	ptok.plugin.eventSystem.on( 'ptok.window.root.init', container.manifest.id, __exampleMenu.onInitRoot )
