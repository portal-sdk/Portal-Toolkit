import unittest.mock

import ptok.util.i18n as i18n


def mock( data: dict[ str, i18n.Translation ] ):
	def wrapper( func ):
		def wrepper( self ) -> None:
			i18n.__current = data
			func( self )
			i18n.__current = { }

		return wrepper

	return wrapper


class I18nTestCase( unittest.TestCase ):
	@mock({
		'test.avoid': i18n.Translation('A dict literal is declared with \\{}', 'core')
	})
	def test_simple_translation( self ):
		self.assertEqual( 'A dict literal is declared with \\{}', i18n.tr( 'test.avoid' ) )

	@mock({
		'test.avoid.double': i18n.Translation('Something \\{\\{}}', 'core')
	})
	def test_simple_translation( self ):
		self.assertEqual( 'Something {{}}', i18n.tr( 'test.avoid.double' ) )

	@mock({
		'test.simple': i18n.Translation('Simple', 'core')
	})
	def test_simple_translation( self ):
		self.assertEqual( 'Simple', i18n.tr( 'test.simple' ) )

	@mock({
		'test.first': i18n.Translation('First {test.second}', 'core'),
		'test.second': i18n.Translation('Second', 'core')
	})
	def test_single_curly( self ):
		self.assertEqual( 'First Second', i18n.tr( 'test.first' ) )

	@mock({
		'test.double': i18n.Translation('Number is {{ctx.num}}', 'core')
	})
	def test_double_curly_dict( self ):
		self.assertEqual( 'Number is 99', i18n.tr( 'test.double', ctx={ 'num': 99 } ) )

	@mock({
		'test.double': i18n.Translation('Number is {{ctx.num}}', 'core')
	})
	def test_double_curly_obj( self ):
		class Context:
			num = 99

		self.assertEqual( 'Number is 99', i18n.tr( 'test.double', ctx=Context() ) )

	@mock({
		'test.double.double': i18n.Translation('{{ctx.name}} is {{ctx.name.len}} chars long', 'core')
	})
	def test_double_curly_dict_obj( self ):
		class Named:
			def __init__( self, name: str ) -> None:
				self.name = name

			@property
			def len( self ) -> int:
				return len( self.name )

			def __str__( self ) -> str:
				return self.name

		self.assertEqual( 'Luke is 4 chars long', i18n.tr( 'test.double.double', ctx={ 'name': Named( 'Luke' ) } ) )


if __name__ == '__main__':
	unittest.main()
